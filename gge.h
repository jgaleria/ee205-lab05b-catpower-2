///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Joshua Galeria jgaleria@hawaii.edu
/// @date 2/12/2022
///////////////////////////////////////////////////////////////////////////////

#pragma once 

const double GASOLINE_IN_A_JOULE  = 1/(1.213e8);
const char   GASOLINE             = 'g';

extern double fromGasolineToJoule ( double gasoline );
extern double fromJouleToGasoline ( double joule );

